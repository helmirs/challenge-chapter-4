class GameSuit {
  constructor() {
    this.result = document.getElementById("result");
    this.playerOption = document.querySelectorAll("#player-area button");
    this.computerOption = document.querySelectorAll("#computer-area button");
    this.reset = document.getElementById("reset");
  }

  // result
  resultText(text) {
    this.result.textContent = text;
  }

  // player 1
  playerChoice(choice) {
    const computerChoice = this.getComputerChoice();

    this.playerOption.forEach((option) => {
      option.classList.remove("selected");
    });

    this.computerOption.forEach((option) => {
      option.classList.remove("selected");
    });

    this.result.classList.add("result-board");
    result.style.color = "#FFFFFF";

    document.getElementById(choice).classList.add("selected");
    document.getElementById(`comp-${computerChoice}`).classList.add("selected");

    //calculate winner
    if (choice === computerChoice) {
      this.resultText("DRAW");
      result.classList.add("result-draw");
    } else if (
      (choice === "rock" && computerChoice === "scissors") ||
      (choice === "paper" && computerChoice === "rock") ||
      (choice === "scissors" && computerChoice === "paper")
    ) {
      this.resultText("PLAYER 1 WIN");
      result.classList.remove("result-draw");
    } else {
      this.resultText("COM WIN");
      result.classList.remove("result-draw");
    }
  }

  // computer
  getComputerChoice() {
    const choices = ["rock", "paper", "scissors"];
    const randomIndex = Math.floor(Math.random() * choices.length);
    return choices[randomIndex];
  }

  // reset
  resetGame() {
    this.resultText("VS");
    result.style.color = "#BD0000";
    result.classList.remove("result-board");
    result.classList.remove("result-draw");

    this.playerOption.forEach((option) => {
      option.classList.remove("selected");
    });

    this.computerOption.forEach((option) => {
      option.classList.remove("selected");
    });
  }
}

//instance gamesuit class
const game = new GameSuit();

// event listeners buttons
document.getElementById("rock").addEventListener("click", () => {
  game.playerChoice("rock");
});

document.getElementById("paper").addEventListener("click", () => {
  game.playerChoice("paper");
});

document.getElementById("scissors").addEventListener("click", () => {
  game.playerChoice("scissors");
});

game.reset.addEventListener("click", () => {
  game.resetGame();
});

result.style.color = "#BD0000";
